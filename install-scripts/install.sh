# Making filesystems and setting swap
mkfs.fat -F32 /dev/nvme0n1p2
mkfs.xfs /dev/nvme0n1p4
mkswap /dev/nvme0n1p3
swapon /dev/nvme0n1p3

# Mounting rootfs
mount /dev/nvme0n1p4 /mnt/gentoo/

# Entering rootfs
cd /mnt/gentoo/

# Downloading Stage 3 Hardened
wget https://bouncer.gentoo.org/fetch/root/all/releases/amd64/autobuilds/20210611T113421Z/hardened/stage3-amd64-hardened-20210611T113421Z.tar.xz

# Unpacking it
tar xpvf stage3-amd64-hardened-20210611T113421Z.tar.xz --xattrs-include='*.*' --numeric-owner

# Removing it
rm stage3-amd64-hardened-20210611T113421Z.tar.xz

# Setting my make.conf
cp $HOME/make.conf etc/porage/make.conf

# Moving repos.conf
mkdir --parents etc/portage/repos.conf
cp usr/share/portage/config/repos.conf etc/portage/repos.conf/gentoo.conf

# Copying resolv.conf
cp --derefence /etc/resolv.conf etc/

# Preparing to chroot
mount --types proc /proc proc/
mount --rbind /sys sys
mount --make-rslave sys
mount --rbind /dev/ dev
mount --make-rslave dev

# Chrooting in
chroot /mnt/gentoo /bin/bash
source /etc/profile

# Mounting extra stuff
mount /dev/nvme0n1p2 /boot

# WebRsyncing
emerge-webrsync

# Updating
emerge --verbose --update --deep --newuse @world

# Installing vim
emerge -q app-editors/vim

# Setting timezone
echo "America/New_Yor" > /etc/timezone
emerge --config sys-libs/timezone-data
echo "en_US.UTF-8 UTF-8" >> /etc/locale-gen
locale-gen
eselect locale set 4
env-update && source /etc/profile

# Kernel setup
emerge -q --autounmask sys-kernel/gentoo-sources genkernel
emerge -q app-arch/lzop app-arch/lz4
cd /usr/src/linux/
# User will have to set up kernel from here manually
make menuconfig
make && make modules_install && make install
mv /usr/src/linux* /usr/src/linux
genkernel --install --kernel-config=/usr/src/linux/.config initramfs

# Setting Hostname
echo 'hostname="gentoo"' > /etc/conf.d/hostname

# Setting up dhcp
emerge --noreplace --quiet net-misc/netifrc
echo 'config_wlp0s20f3="dhcpcd"' > /etc/conf.d/net
emerge net-misc/dhcpcd
cd /etc/init.d/
ln -s net.lo net.wlp0s20f3
rc-update add net.wlp0s20f3 default

# Getting blkids for later
nvme0n1p2 = blkid -s UUID -o value /dev/nvme0n1p2
nvme0n1p3 = blkid -s UUID -o value /dev/nvme0n1p3
nvme0n1p4 = blkdi -s UUID -o value /dev/nvme0n1p4

# Installing grub
emerge --verbose sys-boot/grub:2
grub-install --target=x86_64-efi --efi-directory=/boot --removable
grub-mkconfig -o /boot/grub/grub.cfg

# Editing fstab
echo "$nvme0n1p2 	/boot/efi	vfat	defaults	0 2" >> /etc/fstab
echo "$nvme0n1p3	none	swap	sw	0 0" >> /etc/fstab
echo "$nvme0n1p4	/	xfs	noatime	0 1" >> /etc/fstab

# Adding sudo
emerge -q app-admin/sudo

